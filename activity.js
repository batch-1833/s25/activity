// 2. Use the count operator to count the total number of fruits on sale.


db.fruits.aggregate( [
	{$match: {"onSale": true}},
   { $group: {"_id": "supplier_id", fruitsOnSale: { $sum: 1 } } },
   { $project: { "_id": 0 } }
] )



// 3. Use the count operator to count the total number of fruits with stock more than 20.

db.fruits.aggregate( [
// 	{$match: {"stocks": true}},
   { $match: {"stocks": { $gte: 20 } } },
   { $count: "enoughStocks" } 
] );


// 4. Use the average operator to get the average price of fruits onSale per supplier.


// db.fruits.aggregate([
//      {$match: {"onSale": true}},
//        {$group:{"_id": "supplier_id", average_price: {$avg: "$price"}}
      
//          }
     
//    ]
// )

db.fruits.aggregate(
   [
     {
       $group:
         {
           "_id": "$onSale",
           "avgAmount": { $avg: { $multiply: [ "$price", "$stocks" ] } },
           "average_price": { $avg: "$price" }
         }
     }
   ]
)

// 5. Use the max operator to get the highest price of a fruit per supplier.

db.fruits.aggregate([
     {$match: {"onSale": true}},
       {$group:{"_id": "supplier_id", max_price: {$max: "$price"}}
       
           // {$project: {"_id": 0}}
         }
     
   ]
)

// 6. Use the min operator to get the lowest price of a fruit per supplier.


db.fruits.aggregate([
     {$match: {"onSale": true}},
       {$group:{"_id": "supplier_id", min_price: {$min: "$price"}}
       
           // {$project: {"_id": 0}}
         }
     
   ]
)